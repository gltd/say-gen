"""

`saysynth.core` contains Classes for generating and musical passages in Apple's SpeechSynthesis DSL.

<center><img src="/assets/img/coffee.png"></img></center>

"""
from .arp import Arp  # noqa: F401
from .chord import Chord  # noqa: F401
from .font import Font  # noqa: F401
from .lyrics import Lyrics  # noqa: F401
from .midi_track import MidiTrack  # noqa: F401
from .note import Note  # noqa: F401
from .segment import Segment  # noqa: F401
from .word import Word  # noqa: F401
