"""
This module exists to sync the version
returned via `sy version` to that which is
in `setup.py`.
"""
VERSION = "1.0.11"
"""
The current version of `saysynth`
"""
