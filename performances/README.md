`saysynth` peformances
======================

`saysynth` sequences/files played at live shows:

- [2022-07-24 @ Pageant Space](2022-07-24/)
- [2022-08-24 @ Trans Pecos](2022-08-24/)
- [2022-08-24 @ Wonderville](2022-09-17/)
- [2022-12-19 @ Wonderville](2022-12-19/)

